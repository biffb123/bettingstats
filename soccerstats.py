import urllib.request
from bs4 import BeautifulSoup
import datetime
from tkinter import *
#from mycsv import createcsvfile
#from mycsv import appendcsvline

#add some code to get league position

def createcsvfile(file):

    csv = open(file,'w')
    csv.close()
    
def appendcsvline(file,data,separator):

    csv = open(file,'a')

    output = ''
    
    for item in data:
        if not output:
            output = item
        else:
            output = output + separator + item

    print(output)
    csv.write(output+'\n')
    csv.close()

def appendscreenline(data,separator):

    output = ''
    
    for item in data:
        if not output:
            output = item
        else:
            output = output + separator + item
         
    outputtext.insert(INSERT,output)
    outputtext.insert(INSERT,'\n')
    outputtext.see(END)
    outputtext.update_idletasks()

def getstats(link,league,hometeam,awayteam,kickoff,market,percentage):
    try:

        debug = 'N'
        outcome = '?'
        homegoals = 0
        awaygoals = 0
        
        response = urllib.request.urlopen(link)
       
        html = response.read()
        stats = BeautifulSoup(html)
        bet = 'Y'

        output = [league,hometeam,awayteam,kickoff]
        homepositions = []
        awaypositions = []
        
        if debug == 'Y':
            print('getstats() - HTML',stats)
        for tr in stats.find_all('tr',{'class':True}):
            if debug == 'Y':
                print('getstats() - Looping through tr',tr)
            textlist = []
            type = tr.get('class')
            if str(type) == "['trow2']" or str(type) == "['trow3']" or str(type) == "['headingblue']":
                for td in tr.find_all('td'):
                    if debug == 'Y':
                        print('Looping through td',td)
                    try:
                        text = td.text.replace(u'\xa0',u'')
                        text = text.replace('(A)','')
                        text = text.replace('(B)','')
                        text = text.replace('(C)','')
                        text = text.replace('(D)','')
                        text = text.replace('(E)','')
                        text = text.replace('(F)','')
                        text = text.replace('(G)','')
                        text = text.replace('(H)','')
                        text = text.replace('%','')
                        text = text.replace('-','0')
                        textlist.append(text)
                    except:
                        b = td.find('b')
                        try:
                            textlist.append(b.string.strip())
                        except:
                            textlist.append('?')
                if len(textlist) > 2:
                    if 'Games over' in textlist[2]:
                        output.append(textlist[0])
                        output.append(textlist[1])
                        output.append(textlist[4])
                        output.append(textlist[3])
                        if market in textlist[2] and market != '':
                            if float(textlist[0]) <= percentage:
                            #and float(textlist[0]) <= percentage and float(textlist[1]) <= percentage and float(textlist[3]) <= percentage and float(textlist[4]) <= percentage:
                                bet = 'Y'
                            else:
                                bet = 'N'
                    if textlist[2] == 'OUTCOMES':
                        outcome = 'Y'
                    if textlist[2] == 'HALF0TIME':
                        outcome = 'N'
                    if  textlist[2] == 'Total':
                        if outcome == 'Y':
                            homegoals = textlist[1]
                            awaygoals = textlist[3]
                            outcome == 'N'

            if str(type) == "['trow7']":
                for td in tr.find_all('td'):
                    homepositions.append(td.text.strip())

            if str(type) == "['trow5']":
                for td in tr.find_all('td'):
                    awaypositions.append(td.text.strip())
                            
        output.append(str(homegoals))
        output.append(str(awaygoals))

        if len(homepositions) > 0:
            output.append(homepositions[0])
        else:
            output.append('?')

        if len(homepositions) > 0:
            output.append(awaypositions[14])
        else:
            output.append('?')
            
        if bet == 'N':
            output = []
        
        return output
    
    except IOError:
        return 'Link Failed'

def getmatches(soup):

    rows =9
    columns= 2
    matchdays = [[0 for x in range(columns)] for x in range(rows)]

    td = soup.find('tr',{'class':'trow2'})

    currentrow = 0

    for a in td.find_all('a',{'href':True}):
        link = a.get('href')
        u = a.find('u')
        matchday = u.string
        if '.' in matchday:
            matchdays[currentrow][0] = link
            matchdays[currentrow][1] = matchday
            currentrow = currentrow + 1

    return matchdays

def soccerstats():

    #clear the content of outputtext
    outputtext.delete(0.0, END)

    extractcount = 0
    fixturecount = 0
    totalfixturecount = 0
    
    #check user import from the widgets
    if len(matchlist.curselection()) != 0:
        link = 'http://www.soccerstats.com/'+matches[matchlist.curselection()[0]][0]
        file = 'soccerstats '+matches[matchlist.curselection()[0]][1]+'.csv'
        if len(marketlist.curselection()) != 0:
            market = marketlist.get(marketlist.curselection())
            percentage = percentscale.get()
            message = ['Searching for fixtures: ' + matches[matchlist.curselection()[0]][1] +' - Market : ' + market + ' - Percentage (<=) : ' + str(percentage) + '%']
        else:
            market = ''
            percentage = 0
            message = ['Searching for fixtures:' + matches[matchlist.curselection()[0]][1]]
    else:
        market = ''
        percentage = 0
        link = 'http://www.soccerstats.com/matches.asp'
        message = ['Searching for fixtures: Today']
        file = 'soccerstats '+matches[0]+'.csv'

    appendscreenline(['Connecting to ' + link],',')
    try:
        response = urllib.request.urlopen(link)
        appendscreenline(['Connection successful'],',')
        html = response.read()
        soup = BeautifulSoup(html)
 
        appendscreenline(message,',')
        
        headings = ['LEAGUE','HOME TEAM','AWAY TEAM','KICK OFF','H/T 2.5','H/T OVERALL 2.5','A/T 2.5','A/T OVERALL 2.5','H/T 3.5','H/T OVERALL 3.5','A/T 3.5','A/T OVERALL 3.5','H/T 4.5','H/T OVERALL 4.5','A/T 4.5','A/T OVERALL 4.5','H/T GAMES','A/T GAMES','H/T POS','A/T POS','LINK']

        appendscreenline(['Creating "' + file + '"'],',')
        createcsvfile(file)

        appendcsvline(file,headings,',')
    
        for tr in soup.find_all('tr',{'class':True}):
            textlist=[]
            text = []

            type = tr.get('class')
            if str(type) == "['even']":
                img = tr.find('img')
                try:
                    league = img.get('alt')
                except:
                    u = tr.find('u')
                    league = u.string.strip()
            elif str(type) == "['odd']":
                for td in tr.find_all('td'):
                    outputcount = 0
                    if str(td.string) != 'None':
                        textlist.append(td.string.strip())
                    else:
                        font = td.find('font')
                        try:
                            textlist.append(font.string.strip())
                        except:
                            textlist.append('?')
                    for a in td.find_all('a',{'href':True}):
                        link = a.get('href')
                        if 'pmatch' in link:
                            fixturecount = fixturecount + 1
                            text = getstats('http://www.soccerstats.com/'+link,league,textlist[6],textlist[8],textlist[7],market,percentage)
                            text.append('http://www.soccerstats.com/'+link)
                if len(textlist) == 16:
                    totalfixturecount = totalfixturecount + 1
                    if len(text) > 0:
                        extractcount = extractcount + 1
                        appendscreenline(['Extracting stats.... League: ' + league + ' - Fixture: ' + textlist[6] + ' Vs ' + textlist[8]],',')
                        appendcsvline(file,text,',')

        appendscreenline(['Extracted: ' + str(extractcount) + ' of ' + str(fixturecount) + ' fixtures'],',')
        appendscreenline(['Unable to extract ' + str(totalfixturecount - totalfixturecount) + ' fixture(s) - no stats available'],',')
        return

    except IOError:
        appendscreenline(['Connection Failed'],',')   
        return

def close():
    root.destroy()
    return

root = Tk()

response = urllib.request.urlopen('http://www.soccerstats.com/matches.asp')
html = response.read()
mainsoup = BeautifulSoup(html)

    
root.title("Soccer Stats")
root.geometry("270x900")
root.minsize(width=900,height=270)
root.maxsize(width=900,height=270)

matchlabel = Label(text="Match")
matchlabel.place(x=10,y=10)

matchlist = Listbox(root,height=6,width=25,exportselection=0)

matches = getmatches(mainsoup)

current = 0
for match in matches:
    matchlist.insert(current,matches[current][1])
    current = current + 1

matchlist.place(x=70,y=10)

marketlabel = Label(text="Market")
marketlabel.place(x=10,y=120)

marketlist = Listbox(root,height=3,width=25,exportselection=0)
marketlist.insert(1,'2.5')
marketlist.insert(2,'3.5')
marketlist.insert(3,'4.5')
marketlist.place(x=70,y=120)

outputtext = Text(root,height = 24,width = 80,font=("Lucida Console", 8),bg='Black',fg='Green')
outputtext.place(x=315,y=1)

scroll = Scrollbar(command=outputtext.yview, orient=VERTICAL)
scroll.config(command=outputtext.yview)
outputtext.configure(yscrollcommand=scroll.set)
scroll.pack(side="right", fill="y", expand=False)

percentlabel = Label(text="Percent")
percentlabel.place(x=10,y=170)

percent = 0
percentscale = Scale(root, orient=HORIZONTAL,length=150)
percentscale.place(x=70,y=170)



statbutton = Button(root,text='Get Stats',command=soccerstats)
statbutton.configure(width = 10)
statbutton.place(x=65,y=220)

quitbutton = Button(root,text='  Quit  ',command=close)
quitbutton.configure(width = 10)
quitbutton.place(x=150,y=220)


root.mainloop()
