import urllib.request
from bs4 import BeautifulSoup
import datetime
from tkinter import *


#add some code to get league position

def createcsvfile(file):

    csv = open(file,'w')
    csv.close()
    
def appendcsvline(file,data,separator):

    csv = open(file,'a')

    output = ''
    
    for item in data:
        if not output:
            output = item
        else:
            output = output + separator + item

    print(output)
    csv.write(output+'\n')
    csv.close()

def appendscreenline(data,separator):

    output = ''
    
    for item in data:
        if not output:
            output = item
        else:
            output = output + separator + item
         
    outputtext.insert(INSERT,output)
    outputtext.insert(INSERT,'\n')
    outputtext.see(END)
    outputtext.update_idletasks()

def getstats(link,team):
    try:

        response = urllib.request.urlopen(link)
       
        html = response.read()
        stats = BeautifulSoup(html)

        one = 0
        two = 0
        three = 0
        four = 0
        five = 0
        six = 0
        seven = 0
        eight = 0
        nine = 0
        ten = 0
        greaterthanten = 0
        gametotal = 0

        for li in stats.find_all('li'):
            textlist = [team]
            for div in li.find_all('div'):
                type = div.get('class')
                if str(type) == "['score']":
                    if '-' in div.text:
                        totalruns = int(div.text[0:div.text.index('-')]) + int(div.text[div.text.index('-')+1:len(div.text)])
                        if totalruns == 1:
                            one = one + 1
                        elif totalruns == 2:
                            two = two + 1
                        elif totalruns == 3:
                            three = three + 1
                        elif totalruns == 4:
                            four = four + 1
                        elif totalruns == 5:
                            five = five + 1
                        elif totalruns == 6:
                            six = six + 1
                        elif totalruns == 7:
                            seven = seven + 1
                        elif totalruns == 8:
                            eight = eight + 1
                        elif totalruns == 9:
                            nine = nine + 1
                        elif totalruns == 10:
                            ten = ten + 1
                        elif totalruns > 10:
                            greaterthanten = greaterthanten + 1
                        gametotal = gametotal + 1

        #four
        textlist.append(str(round((four/gametotal)*100,2)))
        #greater than four
        textlist.append(str(round(((five+six+seven+eight+nine+ten+greaterthanten)/gametotal)*100,2)))
        #less than five
        textlist.append(str(round(((one+two+three+four)/gametotal)*100,2)))
        #five
        textlist.append(str(round((five/gametotal)*100,2)))
        #greater than five
        textlist.append(str(round(((six+seven+eight+nine+ten+greaterthanten)/gametotal)*100,2)))
        #less than six
        textlist.append(str(round(((one+two+three+four+five)/gametotal)*100,2)))
        #six
        textlist.append(str(round((six/gametotal)*100,2)))
        #greater than six
        textlist.append(str(round(((seven+eight+nine+ten+greaterthanten)/gametotal)*100,2)))
        #less than seven
        textlist.append(str(round(((one+two+three+four+five+six)/gametotal)*100,2)))
        #seven
        textlist.append(str(round((seven/gametotal)*100,2)))
        #greater than seven
        textlist.append(str(round(((eight+nine+ten+greaterthanten)/gametotal)*100,2)))
        #less than eight
        textlist.append(str(round(((one+two+three+four+five+six+seven)/gametotal)*100,2)))
        #eight
        textlist.append(str(round((eight/gametotal)*100,2)))
        #greater than eight
        textlist.append(str(round(((nine+ten+greaterthanten)/gametotal)*100,2)))
        #less than nine
        textlist.append(str(round(((one+two+three+four+five+six+seven+eight)/gametotal)*100,2)))
        #nine
        textlist.append(str(round((nine/gametotal)*100,2)))
        #greater than nine
        textlist.append(str(round(((ten+greaterthanten)/gametotal)*100,2)))
        #less than ten
        textlist.append(str(round(((one+two+three+four+five+six+seven+eight+nine)/gametotal)*100,2)))
        #ten
        textlist.append(str(round((ten/gametotal)*100,2)))
        #greater than ten
        textlist.append(str(round((greaterthanten/gametotal)*100,2)))
        #total of games
        textlist.append(str(gametotal))
        #add the link at the end
        textlist.append(link)
        return textlist
    
    except IOError:
        return 'Link Failed'

def baseballstats():

    #clear the content of outputtext
    outputtext.delete(0.0, END)

    link = 'http://www.espn.go.com/mlb/history/season'
    file = 'baseballstats ' + str(datetime.date.today()) + '.csv'
    message = ['Searching for stats']

    appendscreenline(['Connecting to ' + link],',')
    try:
        response = urllib.request.urlopen(link)
        appendscreenline(['Connection successful'],',')
        html = response.read()
        soup = BeautifulSoup(html)
 
        appendscreenline(message,',')
        
        headings = ['TEAM','4 RUNS','>4 RUNS','<5 RUNS','5 RUNS','>5 RUNS','<6 RUNS','6 RUNS','>6 RUNS','<7 RUNS','7 RUNS','>7 RUNS','<8 RUNS','8 RUNS','>8 RUNS','<9 RUNS','9 RUNS','>9 RUNS','< 10 RUNS','10 RUNS','>10 RUNS','TOTAL']

        appendscreenline(['Creating "' + file + '"'],',')
        createcsvfile(file)

        appendcsvline(file,headings,',')
    
        for tr in soup.find_all('tr'):
            textlist=[]
            text = []

            for td in tr.find_all('td'):
                outputcount = 0
                if str(td.string) != 'None':
                    textlist.append(td.string)
                for a in td.find_all('a',{'href':True}):
                    link = a.get('href')
            if len(textlist) == 10 and textlist[0] != 'EAST' and textlist[0] != 'WEST' and textlist[0] != 'CENTRAL':
                appendscreenline(['Extracting stats....: ' + textlist[0]],',')
                textlist.append(link)
                text = getstats(link,textlist[0])
                appendcsvline(file,text,',')
                
        return

    except IOError:
        appendscreenline(['Connection Failed'],',')   
        return

def close():
    root.destroy()
    return

root = Tk()

response = urllib.request.urlopen('http://www.soccerstats.com/matches.asp')
html = response.read()
mainsoup = BeautifulSoup(html)

    
root.title("Baseball Stats")
root.geometry("270x900")
root.minsize(width=500,height=310)
root.maxsize(width=500,height=310)

outputtext = Text(root,height = 24,width = 80,font=("Lucida Console", 8),bg='Black',fg='Green')
outputtext.place(x=1,y=1)

scroll = Scrollbar(command=outputtext.yview, orient=VERTICAL)
scroll.config(command=outputtext.yview)
outputtext.configure(yscrollcommand=scroll.set)
scroll.pack(side="right", fill="y", expand=False)

statbutton = Button(root,text='Get Stats',command=baseballstats)
statbutton.configure(width = 10)
statbutton.place(x=170,y=275)

quitbutton = Button(root,text='  Quit  ',command=close)
quitbutton.configure(width = 10)
quitbutton.place(x=260,y=275)


root.mainloop()
